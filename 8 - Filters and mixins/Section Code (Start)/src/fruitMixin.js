export const fruitMixin = {
  data() {
    return {
      filterFruit: "",
      fruits: ["Banana", "Apple", "Mango", "Melon"]
    };
  },
  computed: {
    filteredFruits() {
      return this.fruits.filter(element => {
        return element.match(this.filterFruit);
      });
    }
  }
}
