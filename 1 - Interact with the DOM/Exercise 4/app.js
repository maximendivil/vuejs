new Vue({
  el: '#exercise',
  data: {
    effectClass: '',
    userClass: '',
    activeClass: '',
    disabledClass: '',
    userStyle: '',
    progress: 0
  },
  computed: {
    progressStyle: function () {
      return {
        height: '25px',
        width: this.progress + 'px',
        maxWidth: '100px'
      };
    }
  },
  methods: {
    startEffect: function () {
      var vi = this;
      setInterval(() => {
        vi.effectClass = vi.effectClass == 'highlight' ? 'shrink' : 'highlight';
      }, 1000);
    },
    startProgress: function () {
      var vi = this;
      setInterval(() => {
        vi.progress += 10;
      }, 1000);
    }
  }
});
