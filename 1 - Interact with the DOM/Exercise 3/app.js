new Vue({
  el: '#exercise',
  data: {
    value: 0,
    timeout: 5000
  },
  computed: {
    result: function () {
      return this.value < 37 ? 'not there yet' : 'done';
    }
  },
  watch: {
    result: function () {
      var vi = this;
      setTimeout(function () {
        vi.value = 0;
      }, vi.timeout);
    }
  }
});
