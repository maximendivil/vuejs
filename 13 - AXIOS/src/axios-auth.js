import axios from 'axios'

export default axios.create({
  baseURL: 'https://vuejs-axios-1a6fd.firebaseio.com/'
})
