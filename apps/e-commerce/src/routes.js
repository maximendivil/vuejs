import ProductsGrid from "./components/ProductsGrid.vue"
import SignIn from "./components/SignIn.vue"
import SignUp from "./components/SignUp.vue"
import ProductDetail from "./components/ProductDetail.vue"
import Cart from "./components/Cart.vue"

export const routes = [
  { path: '', component: ProductsGrid },
  { path: '/sign-in', component: SignIn },
  { path: '/sign-up', component: SignUp },
  { path: '/products/:id', component: ProductDetail },
  { path: '/cart', component: Cart }
]
