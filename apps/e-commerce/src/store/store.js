import Vue from 'vue'
import Vuex from 'vuex'
import cart from './modules/cart'

Vue.use(Vuex)

export const store = new Vuex.Store({
  modules: {
    cart
  },
  state: {
    filter: ''
  },
  getters: {
    filter: state => state.filter
  },
  mutations: {
    updateFilter: (state, payload) => state.filter = payload
  },
  actions: {
    updateFilter: ({ commit }, payload) => commit('updateFilter', payload)
  }
});
