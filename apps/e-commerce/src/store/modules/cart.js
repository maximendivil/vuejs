const state = {
  items: []
};

const getters = {
  items: state => state.items,
  itemsCount: state => state.items.length,
  totalQuantity: state => state.items.reduce((total, item) => total + item.quantity, 0)
};

const mutations = {
  add: (state, payload) => {
    const cartItem = state.items.find(item => item.id === payload.id);
    if (cartItem) {
      cartItem.quantity += payload.quantity;
    } else {
      state.items = [...state.items, payload];
    }
  },
  remove: (state, payload) => {
    state.items = state.items.filter(item => item.id !== payload);
  }
};

const actions = {
  add: ({ commit }, payload) => commit('add', payload),
  remove: ({ commit }, payload) => commit('remove', payload)
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
