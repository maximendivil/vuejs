const PRODUCTS = [
  {
    id: 1,
    name: "Superliga ball 2020",
    description: "Official ball for the 19/20 season of the Argentine soccer Superliga",
    img: "https://assets.adidas.com/images/w_280,h_280,f_auto,q_auto:sensitive/77c80e2264ce4bca995aa9f200ed9db4_9366/minipelota-argentina-19.jpg"
  },
  {
    id: 2,
    name: "PlayStation 4 Pro (PS4)",
    description: "The ultimate home entertainment center starts with PlayStation™. Whether you’re into gaming, HD movies, television, music, or all of the above, PlayStation™ offers something great for everyone.",
    img: "https://images-na.ssl-images-amazon.com/images/I/51w3D9iVS1L._AC_SX679_.jpg"
  },
  {
    id: 3,
    name: "Xperia 1 II",
    description: "The Xperia 1 II sets a new bar for speed in a smartphone. It packs the latest cutting-edge technologies and a camera developed with Sony´s Alpha camera engineers to deliver exceptionally fast autofocus in a smartphone. And with a 21:9 CinemaWide 6.5\" 4K HDR OLED display1, you can watch everything in stunning cinema quality.",
    img: "https://www.sony.es/image/bbf2dbeec02feb77b6cac70d79514cc1?fmt=pjpeg&bgcolor=FFFFFF&bgc=FFFFFF&wid=2515&hei=1320"
  }
]

export class ProductService {
  static getAll() {
    return PRODUCTS;
  }

  static getById(id) {
    return PRODUCTS.find(product => product.id == id);
  }
}
