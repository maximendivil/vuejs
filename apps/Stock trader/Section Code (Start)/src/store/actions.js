import Vue from 'vue'

export const loadData = ({ commit }) => {
  Vue.http.get('data.json')
    .then(respose => respose.json())
    .then(data => {
      const stocks = data.stocks;
      const stocksPortfolio = data.stocksPortfolio;
      const funds = data.funds;

      commit('stocks/setStocks', stocks);
      commit('portfolio/setPortfolio', { stocksPortfolio, funds });
    });
}
