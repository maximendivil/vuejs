const state = {
  funds: 10000,
  stocks: []
}

const getters = {
  stocks: (state, getters, rootState, rootGetters) => {
    return state.stocks.map(stock => {
      const record = rootGetters['stocks/stocks'].find(element => element.id == stock.id);

      return {
        id: stock.id,
        quantity: stock.quantity,
        price: record.price,
        name: record.name
      };
    });
  },
  funds: state => state.funds
}

const mutations = {
  buyStock: (state, { stockId, stockPrice, quantity }) => {
    const stock = state.stocks.find(element => element.id == stockId);
    if (stock) {
      stock.quantity += quantity
    } else {
      state.stocks.push({
        id: stockId,
        quantity: quantity
      });
    }
    state.funds -= stockPrice * quantity;
  },
  sellStock: (state, { stockId, stockPrice, quantity }) => {
    const stock = state.stocks.find(element => element.id == stockId);
    if (stock.quantity > quantity) {
      stock.quantity -= quantity
    } else {
      state.stocks.splice(state.stocks.indexOf(stock), 1);
    }
    state.funds += stockPrice * quantity
  },
  setPortfolio: (state, portfolio) => {
    state.funds = portfolio.funds;
    state.stocks = portfolio.stocksPortfolio ? portfolio.stocksPortfolio : [];
  }
}

const actions = {
  buyStock: ({ commit }, order) => commit('buyStock', order),
  sellStock: ({ commit }, stock) => commit('sellStock', stock)
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
