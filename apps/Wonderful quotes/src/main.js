import Vue from 'vue'
import App from './App.vue'

export const eventBus = new Vue({
  methods: {
    submitQuote(quote) {
      this.$emit('quoteWasSubmited', quote);
    },
    deleteQuote(id) {
      this.$emit('quoteWasDeleted', id);
    }
  }
});

new Vue({
  el: '#app',
  render: h => h(App)
})
