const state = {
  counter: 0
};

const getters = {
  counter: state => state.counter,
  doubleCounter: state => state.counter * 2,
  stringCounter: state => `${state.counter} clicks`
};

const mutations = {
  increment: state => state.counter++,
  decrement: state => state.counter--,
  incrementBy: (state, payload) => state.counter += payload,
  decrementBy: (state, payload) => state.counter -= payload
};

const actions = {
  increment: ({ commit }) => commit('increment'),
  decrement: ({ commit }) => commit('decrement'),
  increment: ({ commit }) => commit('increment'),
  decrement: ({ commit }) => commit('decrement'),
  asyncIncrement: ({ commit }, payload) => {
    setTimeout(() => {
      commit('incrementBy', payload.by);
    }, payload.duration);
  },
  asyncDecrement: ({ commit }, payload) => {
    setTimeout(() => {
      commit('decrementBy', payload.by);
    }, payload.duration);
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
